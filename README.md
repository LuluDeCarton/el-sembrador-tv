<h1>El Sembrador TV</h1>
<p>The project "El Sembrador TV" (with ID n° 10510013) is created for Radio El Sembrador de Chillán for Canal 21 TV in order to facilitate the connection and transmission without using the original website. Currently, it works only for PC.</p>
<p>This is the first version of the website.</p>

<br>

<h2>Warnings</h2>
<p>1.  This is an open license project, as it is still under development to optimize transmissions in a complete and compact way.</p>
<p>2.  This website works best on PC, as it was thought for it. If you want to test the site on your mobile device, there will be no problems, since it has the same goal, but with major differences.</p>

<br>

<h2>Website</h2>
<p>If you want to try, experiment or just enjoy the transmission, you can find the project running <a href="http://tv.radioelsembrador.cl" target="_blank">here</a>.</p>

<br>

<p>NEVER STOP PROGRAMMING.</p>
<p>Luciano Sánchez</p>


<img src="https://loverandom.cl/wp-content/uploads/2018/12/logo-lrcl-dark.png">